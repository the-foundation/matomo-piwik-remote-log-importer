#!/bin/bash
## the following variables are set by an external script that replaces all REPLACE... strings
PIWIK_IMPORT_URL=REPLACEPIWIKURL
PIWIK_TOKEN=REPLACEPIWIKTOKEN
CALLING_HOST=REPLACECALLINGHOST

generate_piwik_id_table() {
  site_id_table=$(echo -n
  curl -s ${PIWIK_IMPORT_URL}'/?module=API&method=SitesManager.getAllSites&format=json&token_auth='${PIWIK_TOKEN} | sed 's/},{/\n/g'|while read thisline;do
    idsite=$(echo $thisline|sed 's/","/\n"/g'|grep -e idsite |cut -d'"' -f4);
    siteurl=$(echo $thisline|sed 's/","/\n"/g'|grep -e main_url |cut -d'"' -f4|sed 's/http\(s\|\):\/\///g' );
  ## all addon domains are fetched with a separate api call
  [ ! -z "${idsite}" ] && [ ! -z "${siteurl}" ] && curl -s ${PIWIK_IMPORT_URL}'/?module=API&method=SitesManager.getSiteUrlsFromId&idSite='${idsite}'&format=xml&token_auth='${PIWIK_TOKEN}|grep '<row>'|sed 's/http\(s\|\):\/\///g;s/<\(\|\/\)row>//g;s/\(\t\| \)//g'|grep -v ^"${siteurl}"$ |sed 's/^/'${idsite}'|/g';
  #finally we return the real url and id
  [ ! -z "${idsite}" ] && [ ! -z "${siteurl}" ] && echo "$idsite|$siteurl";
  done
  )

  echo "$site_id_table"|grep -v ^$ > "/dev/shm/piwik_url_table_${CALLING_HOST}"
}
if [ $# -ne 1 ]; then
  echo "need EXACTLY 1 argment ( the main url to resolve) will return id|domain"

fi

find /dev/shm/ -maxdepth 1 -name "piwik_url_table_${CALLING_HOST}" -mmin 1 |grep -q "piwik_url_table_${CALLING_HOST}" || generate_piwik_id_table


#echo "${site_id_table}"|grep '|'"$1"
cat "/dev/shm/piwik_url_table_${CALLING_HOST}"|grep '|'"$1"
