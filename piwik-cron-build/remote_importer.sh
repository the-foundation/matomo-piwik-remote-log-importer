#!/bin/bash
PIWIK_IMPORT_SCRIPT_PATH="/var/www/html/misc/log-analytics/import_logs.py"
PIWIK_IMPORT_URL=REPLACEPIWIKURL
PIWIK_TOKEN=REPLACEPIWIKTOKEN

mypid=$$



mkfifo /tmp/import_current_${mypid}
if [ $# -eq 0 ]; then
    echo "using matomo auto url detection, to pre set use  SCRIPT_NAME hostname ID "
    ionice -c 3 python3 ${PIWIK_IMPORT_SCRIPT_PATH} --token-auth=${PIWIK_TOKEN}  --exclude-path="/wp-admin*" --exclude-path="/backend/*" --exclude-path="/combine/*" --exclude-path="/themes/*" --exclude-path="/plugins/*" --exclude-path="/robots.txt" --exclude-path="/wp-content/plugins/*" --exclude-path="/wp-content/uploads/*" --exclude-path="/wp-includes/*" --exclude-path="/admin/*" --exclude-path="/fileadmin/*" --exclude-path="/typo3temp/*" --exclude-path="/typo3conf/*"  --exclude-path="/storage/app/*" --exclude-path="/backend/*"  --exclude-path="/favicon*" --exclude-path="/*.png" --exclude-path="/*.svg" --exclude-path="/*.svg" --enable-http-redirects --enable-http-errors  --log-format-name=common_complete --url=${PIWIK_IMPORT_URL} /tmp/import_current_${mypid} |grep -v "lines parsed" &
fi

if [ $# -eq 2 ]; then
    echo "using given host/id: ./import_piwik_logs.sh hostname ID ( $1 $2)"
    ionice -c 3 python3 ${PIWIK_IMPORT_SCRIPT_PATH} --token-auth=${PIWIK_TOKEN}  --exclude-path="/wp-admin*" --exclude-path="/backend/*" --exclude-path="/combine/*" --exclude-path="/themes/*" --exclude-path="/plugins/*" --exclude-path="/robots.txt" --exclude-path="/wp-content/plugins/*" --exclude-path="/wp-content/uploads/*" --exclude-path="/wp-includes/*" --exclude-path="/admin/*" --exclude-path="/fileadmin/*" --exclude-path="/typo3temp/*" --exclude-path="/typo3conf/*"  --exclude-path="/storage/app/*" --exclude-path="/backend/*"  --exclude-path="/favicon*" --exclude-path="/*.png" --exclude-path="/*.svg" --exclude-path="/*.svg" --enable-http-redirects --enable-http-errors  --log-format-name=common_complete --url=${PIWIK_IMPORT_URL}  /tmp/import_current_${mypid} |grep -v "lines parsed" &
fi

cat > /tmp/import_current_${mypid}
wait

rm /tmp/import_current_${mypid}
