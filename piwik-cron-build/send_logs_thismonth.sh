#!/bin/bash

[ -z "$PIWIK_TOKEN" ] && echo "NEED PIWIK TOKEN"
[ -z "$PIWIK_TOKEN" ] && exit 0

[ -z "$PIWIK_IMPORT_URL" ] && echo "NEED PIWIK IMPORT URL"
[ -z "$PIWIK_IMPORT_URL" ] && exit 0

cp /remote_importer.sh /domain-resolver.sh /dev/shm

sed 's/REPLACEPIWIKURL/'${PIWIK_IMPORT_URL//\//\\\/}'/g' -i  /dev/shm/remote_importer.sh  /dev/shm/domain-resolver.sh
sed 's/REPLACEPIWIKTOKEN/'${PIWIK_TOKEN}'/g' -i  /dev/shm/remote_importer.sh  /dev/shm/domain-resolver.sh
sed 's/REPLACECALLINGHOST/'$(hostname -f)'/g' -i  /dev/shm/remote_importer.sh  /dev/shm/domain-resolver.sh


test -f /dev/shm/domain-resolver.sh && mv /dev/shm/domain-resolver.sh "/dev/shm/domain-resolver/"$(hostname -f)".sh"

echo "sending remote scripts to destination"
scp -P ${TARGET_PORT}   /dev/shm/domain-resolver.sh  "/dev/shm/domain-resolver/"$(hostname -f)".sh" ${TARGET_SSH}:/tmp/


if [ $# -ne 2 ]; then
    echo "using matomo auto url detection, to pre set use  SCRIPT_NAME hostname ID "

else
    echo "using given host/id: ./import_piwik_logs.sh hostname ID"
fi

##ssh -p ${TARGET_PORT} ${TARGET_SSH} "/bin/bash /tmp/remote_importer.sh"

for curmaindomain in $(cat /piwik-export-domains |grep -v '^#'|grep -v -e "^www\." -e "^www.\." );do
  echo "sending "${curmaindomain}" to remote"
  ##first we send only our domain, should be auto detected or create
    test -f /var/log/nginx/${curmaindomain}.$(date +%Y-%m).log && echo sending /var/log/nginx/${curmaindomain}.$(date +%Y-%m).log
    test -f /var/log/nginx/${curmaindomain}.$(date +%Y-%m).log &&  (cat /var/log/nginx/${curmaindomain}.$(date +%Y-%m).log  |ssh -p ${TARGET_PORT} ${TARGET_SSH} "/bin/bash /tmp/remote_importer.sh" )

  echo "sending www/www1/www2."${curmaindomain}" to remote"
  ## now resolve the id , as www.something and www1.something fails in auto detection
  echo -n "resolving idsite:"
  domainid=$(ssh -p ${TARGET_PORT} ${TARGET_SSH} "/bin/bash /tmp/domain-resolver.sh ${curmaindomain}" |grep '|'"$curmaindomain"$|cut -d "|" -f1 )
  echo "$domainid"
  if [ ! -z "${domainid}" ];then
  for curdomain in  www.${curmaindomain} www1.${curmaindomain} www2.${curmaindomain};do

    test -f /var/log/nginx/${curdomain}.$(date +%Y-%m).log && echo sending /var/log/nginx/${curdomain}.$(date +%Y-%m).log
    test -f /var/log/nginx/${curdomain}.$(date +%Y-%m).log &&  (cat /var/log/nginx/${curdomain}.$(date +%Y-%m).log  |ssh -p ${TARGET_PORT} ${TARGET_SSH} "/bin/bash /tmp/remote_importer.sh ${curmaindomain} ${domainid}" )
  done
  fi
done
