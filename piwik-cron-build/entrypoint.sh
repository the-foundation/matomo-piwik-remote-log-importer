#!/bin/sh
set -e
echo "|=== init() ==="
echo  "|=>fix permissions:"
time chown piwik -R /home/piwik /var/piwik-locks
time chmod o+w /var/piwik-locks -R
chmod 0600 /home/piwik/.ssh/id_rsa /home/piwik/.ssh/id_rsa.pub
echo "|=============="
echo "|===> main() ->donothingloop"
echo "|=============="
while (true);do
  echo -n "|"$(date -u)"|"$(date);
  ##only run after 0 o'clock (0 am FTW) , then sleep for an hour , in any other case sleep for 10 minutes to start quickly after midnight
  if [ "$(date +%H )" -eq 0 ] ; then
    echo "my time has come";
    su -s /bin/bash -c "cd /;bash send_logs_cron.sh" piwik
    sleep 3600;
  else
    sleep 900;
  fi
  echo debug "ENDLOOP"
done
#exec "$@"
