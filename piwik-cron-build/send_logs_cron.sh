#!/bin/bash

[ -z "$PIWIK_TOKEN" ] && echo "NEED PIWIK TOKEN"
[ -z "$PIWIK_TOKEN" ] && exit 0

[ -z "$PIWIK_IMPORT_URL" ] && echo "NEED PIWIK IMPORT URL"
[ -z "$PIWIK_IMPORT_URL" ] && exit 0


cp /remote_importer.sh /domain-resolver.sh /dev/shm

sed 's/REPLACEPIWIKURL/'${PIWIK_IMPORT_URL//\//\\\/}'/g' -i  /dev/shm/remote_importer.sh  /dev/shm/domain-resolver.sh
sed 's/REPLACEPIWIKTOKEN/'${PIWIK_TOKEN}'/g' -i  /dev/shm/remote_importer.sh  /dev/shm/domain-resolver.sh
sed 's/REPLACECALLINGHOST/'$(hostname -f)'/g' -i  /dev/shm/remote_importer.sh  /dev/shm/domain-resolver.sh

mkdir -p /dev/shm/domain-resolver 2>/dev/null || true
test -f /dev/shm/domain-resolver.sh && mv /dev/shm/domain-resolver.sh "/dev/shm/domain-resolver/"$(hostname -f)".sh"

echo "sending remote scripts to destination"
scp -P ${TARGET_PORT}  /dev/shm/remote_importer.sh  "/dev/shm/domain-resolver/"$(hostname -f)".sh" ${TARGET_SSH}:/tmp/


#if [ $# -ne 2 ]; then
#    echo "using matomo auto url detection, to pre set use  SCRIPT_NAME hostname ID "

#else
#    echo "using given host/id: ./import_piwik_logs.sh hostname ID"
#fi


send_piwik_log() {
  PIWIK_IMPORT_LINES_COUNT=0
  PIWIK_IMPORT_WORKING_DIR="/var/piwik-locks/$1"
  [ ! -z ${PIWIK_IMPORT_WORKING_DIR} ] && test -d ${PIWIK_IMPORT_WORKING_DIR} || mkdir ${PIWIK_IMPORT_WORKING_DIR}
  ## if somebody called us with parameters, we append ${curmaindomain} ${domainid} to the remote importer
  APPENDTHIS=""
  [ ! -z "$1" ] && [ ! -z "$2" ] && [ ! -z "$3" ] && APPENDTHIS=" $2 $3"
  ## if somebody only set the first parameter , we do not append at all since it is auto handled by remote_importer
  ## we need 3 parameters for subdomains , 1: SUBDOMAIN 2:MAINDOMAIN 3: idSite(matomo)

  ### search over month shift
  year=$(date +%Y)
  prevmonth=$(($(date +%m|sed 's/^0\+//g')-1))
  #echo prevbeforeshift ${prevmonth}
  if [ "${prevmonth}" -eq 0 ];then prev=12 ; year=$(("${year}"-1));fi;

  ##this controls the numbers of days we import, it has to be larger than 0
  [ -z "$PIWIK_BACKLOG_DAYS" ] && backlogdays=8
  [ -z "$PIWIK_BACKLOG_DAYS" ] || backlogdays="$PIWIK_BACKLOG_DAYS"


  #backlog=$(for backlogtime in $(seq -${backlogdays} -1);do backlogdate=$(LC_ALL=C date +%d/%b/%Y  --date " ${backlogtime} days" );echo $backlogdate;done  )
  ##command above failed on alpine/busybox due to CRAP implementation of their seq ( fails on negative values)
  backlog=$(for backlogtime in $(/bin/bash -c  "echo $(echo {-${backlogdays}..-1})");do backlogdate=$(LC_ALL=C date +%d/%b/%Y  --date " ${backlogtime} days" );echo $backlogdate;done  )

  echo "------------------------------------------------------------------------------------"
  echo "sending "$1" to remote (prev month: ${prevmonth} | backlog is "$backlog" | |PARAMS: $@ )"
  echo "------------------------------------------------------------------------------------"

  searchprev=false

filelist=$(for backlogtime in $(/bin/bash -c  "echo $(echo {-${backlogdays}..-1})");do 
                                                backlogdate=$(LC_ALL=C date +%Y-%m-%d  --date " ${backlogtime} days" );
                                                ls -1 /var/log/nginx/$1.${backlogdate}".log" 2>/dev/null |grep -q /var/log/nginx/$1.${backlogdate}".log" && echo /var/log/nginx/$1.${backlogdate}".log";done  )

##only search last month for the first (backlogdays-1) days of next month
  if [ "$(date +%d|sed 's/^0//')" -le $((backlogdays-1)) ] ;then
#      echo searching in 
#      #echo 'SEARCHING IN /var/log/nginx/'$1'*'${year}'-'$(date +%m)'*log /var/log/nginx/'$1'*'${year}'-'$(printf %.02d ${prevmonth})'*log'
      searchprev=true
#      #lastmonth=$(for lastmonthtime in $(/bin/bash -c  "echo $(echo {-${backlogdays}..-1})");do lastmonthdate=$(LC_ALL=C date +%d/%b/%Y  --date " ${lastmonthtime} days" );echo $lastmonthdate;done  )
      lastmonth=${backlog}
#      echo lastmonth set to $lastmonth
  else
      echo 'SEARCHING ONLY IN /var/log/nginx/'$1'*'${year}'-'$(date +%m)'*log'
  fi
  echo 'SEARCHING IN FILES: '${filelist};
  echo '############################################'
  echo -n "BUILDING SEARCH TERM: "

  SEARCHTERM=""
  SEARCHTERMPREV=""
  
  ####for searchdate in $backlog  ;do
  ####echo -n "SEARCHING:"${searchdate} "|"
  ####if [ "$searchprev" == "true" ];then
  ####  prev_file_lock=${PIWIK_IMPORT_WORKING_DIR}/lock.${searchdate//\/}-${year}-$(printf %.02d ${prevmonth})
  ####  test -f ${prev_file_lock} && (echo -n "|N: locked by ${prev_file_lock##${PIWIK_IMPORT_WORKING_DIR}} , delete file to reimport | " >&2
  ####                                ) ||  SEARCHTERMPREV="${SEARCHTERMPREV} -e ${searchdate} "
  ####
  ####fi
  ####  ###now only current date
  ####  searchdate=$(LC_ALL=C date +%d/%b/%Y)
  ####  file_lock=${PIWIK_IMPORT_WORKING_DIR}/lock.${searchdate//\/}-${year}-$(date +%m)
  ####  
  ####  test -f ${file_lock} && (echo -n "|N: locked by ${file_lock##${PIWIK_IMPORT_WORKING_DIR}} , delete file to reimport" >&2
  ####                          ) ||  SEARCHTERM="${SEARCHTERM} -e ${searchdate} "
  ####
  ####  done
  #for searchdate in $backlog  ;do
  ####                date  +%Y-%m-%d --date "24 Jan 2020"
  #searchdateminus=$(date  +%Y-%m-%d --date "${searchdate//\// }")
  #
  ##file_lock=${PIWIK_IMPORT_WORKING_DIR}/lock-$1.${searchdateminus}
  ##echo -n " SEARCHING: "${searchdate} "| as |"${searchdateminus}" in "${file_lock}" | " >&2
  ##ls -1  ${file_lock} 2>/dev/null |grep -q ${file_lock} && (echo -n "|N: locked by ${file_lock##${PIWIK_IMPORT_WORKING_DIR}} , delete file to reimport" >&2
  ##                          ) ||  SEARCHTERM="${SEARCHTERM} -e ${searchdate} "
  ##
  ##
  ##done
  
  echo
    
  #echo "SEARCHTERM NOW: "${SEARCHTERM}
  
  echo "|=================================="
  echo "|FEEDING FIFO with"${filelist}
  echo "|=================================="
  #SEARCHTERM=${SEARCHTERM}" "${SEARCHTERMPREV}
  
  #if [ -z "$searchterm" ]; then echo "empty SEARCHTERM";else 
    for infile in ${filelist};do 
    lockfile=${infile//\/var\/log\/nginx\//};lockfile=lock-${lockfile//.log/};
    #echo $lockfile
    locked=no
    ls -1  ${PIWIK_IMPORT_WORKING_DIR}/${lockfile} 2>/dev/null |grep -q ${lockfile} && locked=yes
    if [ "${locked}" = "no" ]; then
    
       #ACTION="ionice -c3 grep -h "${SEARCHTERM}" "${infile}
       ACTION="ionice -c3 cat "${infile}
       
       echo  "TARGET:" ${SEARCHTERM} "|| ACTION:" ${ACTION} '|  pv --rate-limit 1M  ' >&2
       /bin/bash -c "${ACTION}" |  pv --rate --bytes --rate-limit 1M | grep -v -e "UptimeRobot" -e 127.0.0.1 -e wp-config -e wp-admin -e admin-ajax -e "/backend/"| ssh -p ${TARGET_PORT} ${TARGET_SSH} "/bin/bash /tmp/remote_importer.sh ${APPENDTHIS}" 2>&1 |grep -v "0 lines parsed, 0 lines recorded"
       
       echo ;echo -n "LOCKING: "${PIWIK_IMPORT_WORKING_DIR}/${lockfile}
             touch ${PIWIK_IMPORT_WORKING_DIR}/${lockfile}
       #    for searchdate in $backlog  ;do  echo -n "LOCKING" ${searchdate}; touch ${PIWIK_IMPORT_WORKING_DIR}/lock.${searchdate//\/}-${year}-$(date +%m);done
    else 
    echo "$infile locked by ${PIWIK_IMPORT_WORKING_DIR}/${lockfile} , delete to reimport" 
    fi
echo 
    done

  #fi 
echo 


  ## search term is empty if threshold is not met and thus searchprev was false
  ####if [ -z "$SEARCHTERMPREV" ] ;then echo not searching previous month ; else
  ####
  ####    #ACTION="ionice -c3 grep -h "${SEARCHTERMPREV}" /var/log/nginx/$1*${year}-$(printf %.02d ${prevmonth})*log"
  ####    ACTION="ionice -c3 grep -h "${SEARCHTERMPREV}" "${filelist}
  ####    echo  "TARGET:" ${SEARCHTERMPREV} "ACTION:" $ACTION '|  pv --rate-limit 1M  ' >&2
  ####    /bin/bash -c "${ACTION}" |  pv --rate --bytes --rate-limit 1M | grep -v -e "UptimeRobot" -e 127.0.0.1 | ssh -p ${TARGET_PORT} ${TARGET_SSH} "/bin/bash /tmp/remote_importer.sh ${APPENDTHIS}" 2>&1 |grep -v "0 lines parsed, 0 lines recorded"
  ####    echo ;echo -n "LOCKS:"
  ####    for searchdate in $backlog  ;do  echo -n "LOCKING: " ${searchdate}; touch ${PIWIK_IMPORT_WORKING_DIR}/lock.${searchdate//\/}-${year}-$(printf %.02d ${prevmonth});done
  ####    echo
  ####fi
  ####
  ####if [ -z "$SEARCHTERM" ] ;then true ;else
  ####    #ACTION="ionice -c3 grep -h "${SEARCHTERM}" /var/log/nginx/$1*${year}-$(date +%m)*log"
  ####    ACTION="ionice -c3 grep -h "${SEARCHTERMPREV}" "${filelist}
  ####    echo  "TARGET:" ${SEARCHTERMPREV} "ACTION:" $ACTION '|  pv --rate-limit 1M ' >&2
  ####    /bin/bash -c "${ACTION}" |  pv --rate --bytes --rate-limit 1M | grep -v -e "UptimeRobot" -e 127.0.0.1 -e wp-config -e wp-admin -e admin-ajax -e "/backend/"| ssh -p ${TARGET_PORT} ${TARGET_SSH} "/bin/bash /tmp/remote_importer.sh ${APPENDTHIS}"  2>&1 |grep -v "0 lines parsed, 0 lines recorded"
  ####    echo ;echo -n "LOCKS:"
  ####    for searchdate in $backlog  ;do  echo -n "LOCKING" ${searchdate}; touch ${PIWIK_IMPORT_WORKING_DIR}/lock.${searchdate//\/}-${year}-$(date +%m);done
  ####    echo
  ####fi

  echo CLEANING  with find ${PIWIK_IMPORT_WORKING_DIR}/ -type f -name "lock*" -mtime +180 -delete
  find ${PIWIK_IMPORT_WORKING_DIR}/ -type f -name "lock*" -mtime +180 -delete


echo ; } ;



##ssh -p ${TARGET_PORT} ${TARGET_SSH} "/bin/bash /tmp/remote_importer.sh"

### START main()###


for curmaindomain in $(cat /piwik-export-domains |grep -v '^#'|sed 's/^www\.//g'|awk '!x[$0]++' );do
  ##first we send only our domain, should be auto detected or create
  echo "sending ${curmaindomain}"
  send_piwik_log "${curmaindomain}"

  echo "sending www/www1/www2."${curmaindomain}" to remote"
  ## now resolve the id , as www.something and www1.something fails in auto detection
  echo -n "resolving idsite:"
  domainid=$(ssh -p ${TARGET_PORT} ${TARGET_SSH} "/bin/bash /tmp/"$(hostname -f)".sh ${curmaindomain}" |grep '|'"$curmaindomain"$|cut -d "|" -f1 )
  echo "$domainid"
  if [ ! -z "${domainid}" ];then
    for curdomain in www.${curmaindomain} www1.${curmaindomain} www2.${curmaindomain};do
      send_piwik_log "${curdomain}" "${curmaindomain}" ${domainid}
    done
  fi
done


echo "CLEANING LOG FILES FROM +30D"

echo "END $(basename $0)"
