

## Installation/Setup

* git clone https://gitlab.com/the-foundation/matomo-piwik-remote-log-importer.git
* create a `.env` that looks like the following snippet:

```
COMPOSE_PROJECT_NAME=piwik

CONTAINER_NAME=analytics-export.your.webserver.net
PIWIK_IMPORT_SCRIPT_PATH="/var/www/html/misc/log-analytics/import_logs.py"
PIWIK_IMPORT_URL=https://your.piwik.tld
PIWIK_TOKEN=b4d4f33db33f02caffee

TARGET_SSH=www-data@your.piwik.tld
TARGET_PORT=22222

```
---

<a href="https://the-foundation.gitlab.io/">
<h3>A project of the foundation</h3>
<div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/matomo-piwik-remote-log-importer/README.md/logo.jpg" width="480" height="270"/></div></a>
